import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Infid',
      theme: ThemeData(textTheme: GoogleFonts.poppinsTextTheme()),
      darkTheme: ThemeData.dark(),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Infid"),
        ),
        body: Container(
            child: Column(
          children: [
            Text(
              'Vaksinasi COVID-19',
              style: GoogleFonts.poppins(
                  textStyle: TextStyle(
                fontSize: 40,
                fontWeight: FontWeight.bold,
              )),
            ),
            Divider(
              color: Colors.black,
              thickness: 1,
              indent: 20,
              endIndent: 20,
            ),
            Text(
              """Vaksinasi adalah pemberian Vaksin dalam rangka menimbulkan atau meningkatkan kekebalan seseorang secara aktif terhadap suatu penyakit, sehingga apabila suatu saat terpajan dengan penyakit tersebut tidak akan sakit atau hanya mengalami sakit ringan dan tidak menjadi sumber penularan.""",
              style: GoogleFonts.poppins(
                  textStyle: TextStyle(
                fontSize: 20,
              )),
              textAlign: TextAlign.center,
            ),
          ],
        )),
      ),
    );
  }
}
