from django.http.response import HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from .forms import NoteForm
from lab_2.models import Note
# Create your views here.
def index(request):
    notes  = Note.objects.all()
    response = {'notes' : notes}
    return render(request, 'lab4_index.html', response)


def add_notes(request):
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()  # Memasukkan data ke database
            return HttpResponseRedirect('/lab-4/')  # Redirect to /lab-4
    else:
        form = NoteForm()
    return render(request, 'lab4_form.html', {'form': form})

def note_list(request):
    notes  = Note.objects.all()
    response = {'notes' : notes}
    return render(request, 'lab4_note_list.html', response)