from django.forms import ModelForm, fields
from lab_2.models import Note

class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields = "__all__"