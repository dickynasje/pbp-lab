from django.urls import path
from .views import index, add_notes, note_list
urlpatterns = [
    path('', index, name='index'),
    # TODO Add friends path using friend_list Views
    path('add-note', add_notes, name='Add Notes'),
    path('note-list', note_list)
]