from django.http.response import HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from .forms import FriendForm
from lab_1.models import Friend
# Create your views here.
def index(request):
    friends = Friend.objects.all
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url="/admin/login/")
def add_friend(request):
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()  # Memasukkan data ke database
            return HttpResponseRedirect('/lab-3')  # Redirect to /lab-3 when valid

    else:
        form = FriendForm()

    return render(request, 'lab3_form.html', {'form': form})