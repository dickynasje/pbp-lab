from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=512)
    fromWho = models.CharField(max_length=512)
    title = models.CharField(max_length=512)
    message =  models.CharField(max_length=512)
