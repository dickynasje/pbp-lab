from django.urls import path
from .views import index, xmlRequest, jsonRequest

urlpatterns = [
    path('', index, name='index'),
    path('xml', xmlRequest, name='xml'),
    path('json', jsonRequest, name='json')
]