from django.shortcuts import render
from .models import Note
from django.core import serializers
from django.http.response import HttpResponse
# Create your views here.
def index(request):
    notes  = Note.objects.all()
    response = {'notes' : notes}
    return render(request, 'index_lab2.html', response)

def xmlRequest(request):
    notes  = Note.objects.all()
    serializedXML = serializers.serialize('xml', notes)
    return HttpResponse(serializedXML, content_type="application/xml")

def jsonRequest(request):
    notes  = Note.objects.all()
    serializedJSON = serializers.serialize('json', Note.objects.all())
    return HttpResponse(serializedJSON, content_type="application/json")